# Status monitoring for NetMetr backend servers

```
git clone https://gitlab.labs.nic.cz/websites/netmetr-status.git
cd netmetr-status
npm install
```

## Configuration

Edit  `settings.json`.

## Building

```
npm run build
```

Built website will be placed in `build/`.

## Development

Run development server (with live reload):

```
npm start
```

Colors are defined in `src/colors.scss`.
