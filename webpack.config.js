const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  entry: "./src/main.jsx",
  output: {
    path: path.join(__dirname, "public"),
    filename: "netmetr-status.[hash:10].js"
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          presets: [
            [
              "env",
              {
                targets: {
                  browsers: ["last 2 versions", "> 5%", "IE 11"]
                },
                debug: false
              }
            ]
          ],
          cacheDirectory: true,
          plugins: [
            [
              "transform-react-jsx",
              {
                pragma: "h"
              }
            ]
          ]
        }
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader", "sass-loader"]
        })
      },
      {
        test: /\.json$/,
        loaders: ["json-loader"]
      }
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, "build"),
    historyApiFallback: false,
    hot: true,
    inline: true,
    host: "localhost",
    port: 8000
  },
  devtool: "source-map",
  plugins: [
    new HtmlWebpackPlugin({ inject: "head" }),
    new ExtractTextPlugin("netmetr-status.[contenthash:10].css"),
    new webpack.HotModuleReplacementPlugin()
  ],
  stats: {
    colors: true
  }
};
