import { h } from "preact";
import style from "./Server.scss";
import Service from "../Service/Service.jsx";

const Server = ({ title, url, services }) => (
  <div className="server">
    <header>
      <h2>{title}</h2>
      <code>{url}</code>
    </header>
    <div className="services">
      {services.map((service, i) => (
        <Service key={i} name={service} url={url} />
      ))}
    </div>
  </div>
);

export default Server;
