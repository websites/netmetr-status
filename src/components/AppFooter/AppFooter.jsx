import { h } from "preact";
import style from "./AppFooter.scss";

const AppFooter = ({ content }) => (
  <footer className="app-footer">{content}</footer>
);

export default AppFooter;
