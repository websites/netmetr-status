import { h } from "preact";
import style from "./AppLayout.scss";
import AppHeader from "../AppHeader/AppHeader.jsx";
import AppFooter from "../AppFooter/AppFooter.jsx";
import Server from "../Server/Server.jsx";

const AppLayout = ({ settings }) => (
  <div className="app">
    <AppHeader title={settings.appTitle} />
    <div className="content">
      {settings.servers.map(function(server, i) {
        return (
          <Server
            key={i}
            title={server.title}
            url={server.baseUrl}
            services={server.services}
          />
        );
      })}
    </div>
    <AppFooter
      content={
        <p>
          {settings.footerText} | <a href={settings.repoLink}>Git</a>
        </p>
      }
    />
  </div>
);

export default AppLayout;
