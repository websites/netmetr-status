import { h } from "preact";
import style from "./AppHeader.scss";

const AppHeader = ({ title }) => (
  <header className="app-header">
    <h1>{title}</h1>
  </header>
);

export default AppHeader;
