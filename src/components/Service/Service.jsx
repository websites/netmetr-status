import { h, Component } from "preact";
import style from "./Service.scss";
import nanoajax from "nanoajax";
import settings from "../../../settings.json";

export default class Service extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: settings.loadingText
    };
  }

  tick() {
    var serviceUrl = this.props.url + this.props.name + "/version";
    var component = this;
    if (component.state.status === "dead") {
        component.setState({
          text: settings.loadingText
        });
    };
    nanoajax.ajax(
      {
        url: serviceUrl,
        timeout: settings.timeout * 1000
      },
      function(code, responseText) {
        if (code === 200) {
          let response = JSON.parse(responseText);
          component.setState({
            status: "live",
            text: response.version
          });
        } else if (code === 0) {
          component.setState({
            status: "dead",
            text: settings.loadingError
          });
        } else {
          component.setState({
            status: "dead",
            text: "HTTP " + code
          });
        }
      }
    );
  }

  componentDidMount() {
    this.tick.bind(this)();
    this.timer = setInterval(
      this.tick.bind(this),
      settings.updateInterval * 1000
    );
  }

  render() {
    let className = "service";
    if (this.state.status === "live") {
      className += " live";
    } else if (this.state.status === "dead") {
      className += " dead";
    } else {
      className = "service loading";
    }

    return (
      <div className={className}>
        <h3>{this.props.name}</h3>
        <code>{this.state.text}</code>
      </div>
    );
  }
}
