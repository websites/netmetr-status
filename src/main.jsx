// @flow

import settings from "../settings.json";
import { h, render } from "preact";
import AppLayout from "./components/AppLayout/AppLayout.jsx";
import style from "./main.scss";
import Raven from "raven-js";

Raven.config(settings.sentryDSN).install();

const root = document.createElement("div");

try {
  document.addEventListener("DOMContentLoaded", function() {
    document.body.appendChild(root);
    document.title = settings.appTitle;
    render(<AppLayout settings={settings} />, root);
  });
} catch (e) {
  Raven.captureException(e);
}
